using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float verticalSpeed;
    [SerializeField] float shootCoolDownSeconds = 1;
    [SerializeField] int damage = 10;

    private GameManager gameManager;

    #region Movement
    private float input;
    private float height;
    private float verticalBounds;
    #endregion

    #region Shooting
    private ShootProjectile shooting;
    private bool canShoot;
    float shootingTime;
    #endregion

    private void Awake()
    {
        canShoot = true;
        shootingTime = 0;

        shooting = GetComponent<ShootProjectile>();

        if (shooting == null) Debug.LogError("Missing ShootProjectile script");

        height = GetComponent<BoxCollider2D>().size.y;
    }

    private void Start()
    {
        gameManager = GameManager.Instance;
        verticalBounds = gameManager.VerticalBounds - height / 2;
        
        //Debug.Log(verticalBounds);
    }

    void Update()
    {
        input = Input.GetAxisRaw("Vertical");

        if (GameManager.CurrentGameState == GameState.GameStopped && input != 0f) gameManager.StartGame();

        if (gameManager.GameStopped) return;

        HandleShooting();
    }

    #region Shooting Methods
    private void HandleShooting()
    {
        ShootCooldown();

        if (Input.GetButton("Fire1") && canShoot)
            Shoot();
    }

    private void Shoot()
    {
        shooting.Shoot(damage, tag);

        canShoot = false;
        shootingTime = 0;
    }

    private void ShootCooldown()
    {
        if(!canShoot)
        {
            shootingTime += Time.deltaTime;

            if (shootingTime >= shootCoolDownSeconds)
                canShoot = true;
        }
    }
    #endregion

    private void FixedUpdate()
    {
        transform.Translate(Vector2.up * (input * verticalSpeed * Time.fixedDeltaTime));

        float playerX = transform.position.x;


        // Snap player to bounds
        if(transform.position.y > verticalBounds)
        {
            transform.position = new Vector2(playerX, verticalBounds);
        }

        if (transform.position.y < -verticalBounds)
        {
            transform.position = new Vector2(playerX, -verticalBounds);
        }

    }


}
