using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] int maxHealth;
    AudioSource audioSource;

    public int Health { get; private set; }

    public static event UIManager.OnDamageTakenDelegate OnDamageTaken;

    private void Awake()
    {
        Health = maxHealth;
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        GameManager.Instance.Player = this;
    }

    public void TakeDamage(int damage)
    {
        Health -= damage;

        //Debug.Log("Ouch " + Health);
        OnDamageTaken?.Invoke(Health, maxHealth);
        if(Health <= 0)
        {
            Health = 0;
            Die();
        } 

        audioSource.Play();
    }

    private void Die()
    {
        // SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        GameManager.Instance.GameOver(false);
    }

    
}
