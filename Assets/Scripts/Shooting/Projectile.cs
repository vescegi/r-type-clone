using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] Renderer rendererComponent;
    [SerializeField] float pitchRandomizer;

    private GameObject shooter;
    private float speed = 10;
    private int damage = 10;

    private AudioSource audioSource;
    

    private Rigidbody2D rb;
    private Player player;
    private string shooterTag;

    public void Init(GameObject shooter, float speed, int damage, string shooterTag)
    {
        this.shooter = shooter;
        this.speed = speed;
        this.damage = damage;
        this.shooterTag = shooterTag;
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        player = GameManager.Instance.Player;
        
    }

    private void Update()
    {
        Vector2 right = transform.right;
        rb.position += right * (speed * Time.deltaTime);

        if (!rendererComponent.isVisible) GoToQueue();
    }

    private void OnEnable()
    {
        PlayAudio();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("Shot " + collision.gameObject.name);

        Collider2D collided = collision.collider;

        if (collided.CompareTag(shooterTag)) return;


        if (collided.CompareTag("Player"))
        {
            player.TakeDamage(damage);
        }
        else if (collided.CompareTag("Enemy"))
        {
            collided.GetComponent<Enemy>().TakeDamage(damage);
        }
        else if (collided.CompareTag("Beholder"))
        {
            collided.GetComponent<Beholder>().TakeDamage(damage);
        }


        GoToQueue();
    }

    private void GoToQueue()
    {
        ProjectileManager.Instance.AddProjectile(gameObject);
    }

    private void PlayAudio()
    {
        audioSource.pitch = Random.Range(1 - pitchRandomizer, 1 + pitchRandomizer);
        audioSource.Play();
    }
}
