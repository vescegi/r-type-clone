using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootProjectile : MonoBehaviour
{
    private Transform muzzle;



    [SerializeField] private float projectileSpeed;

    public Transform Muzzle { get => muzzle; }

    private void Awake()
    {
        muzzle = transform.Find("Muzzle");
        //if (muzzle == null) Debug.LogError("Missing muzzle");
    }

    public void Shoot(int damage, string shooter)
    {  
        ProjectileManager.Instance.GetProjectile(muzzle, gameObject, damage, shooter, projectileSpeed);
    }
    public void Shoot(int damage, string shooter, Vector2 position)
    {
        muzzle.position = position;

        ProjectileManager.Instance.GetProjectile(muzzle, gameObject, damage, shooter, projectileSpeed);
    }
}
