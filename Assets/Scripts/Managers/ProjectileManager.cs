using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;


public class ProjectileManager : MonoBehaviour
{
    private static ProjectileManager istance;
    public static ProjectileManager Instance
    {
        get => istance;

        private set => istance = value;
    }

    private ConcurrentQueue<GameObject> availableProjectiles;
    

    [SerializeField] GameObject projectilePrefab;
    [SerializeField] GameObject projectileFolder;

    private void Awake()
    {
        istance = this;
        availableProjectiles = new ConcurrentQueue<GameObject>();
    }

    public GameObject GetProjectile(Transform muzzle, GameObject shooter, int damage, string shooterTag, float speed)
    {
        GameObject projectile = null;

        // If there is a projectile ready in the queue, get it from the queue, otherwise generate it
        if (availableProjectiles.Count == 0)
        {
            // Debug.Log("aaaaa");
            projectile = Instantiate(projectilePrefab);
        }
        else
        {
            availableProjectiles.TryDequeue(out projectile);
        }

        // Rotate to the muzzle
        projectile.SetActive(true);
        projectile.transform.position = muzzle.position;
        projectile.transform.rotation = muzzle.rotation;
        projectile.transform.parent = projectileFolder.transform;
        projectile.GetComponent<Projectile>().Init(shooter, speed, damage, shooterTag);


        return projectile;
    }

    public void AddProjectile(GameObject projectile)
    {
        projectile.SetActive(false);
        availableProjectiles.Enqueue(projectile);
    }
}
