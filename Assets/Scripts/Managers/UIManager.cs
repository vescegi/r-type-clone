using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public delegate void OnDamageTakenDelegate(int health, int maxHealth);
    [SerializeField] GameObject healthBar;
    [SerializeField] GameObject bossBar;
    [SerializeField] GameObject moveToStartText;
    [SerializeField] GameObject pausePanel;

    Slider healthBarSlider;
    Slider bossBarSlider;

    private void Start()
    {
        Player.OnDamageTaken += ChangePlayerHealthBar;
        healthBarSlider = healthBar.GetComponent<Slider>();

        Beholder.OnBeholderSpawn += EnableBossBar;
        Beholder.OnDamageTaken += ChangeBossHealthBar;
        bossBarSlider = bossBar.GetComponent<Slider>();

        GameManager.OnGameStart += GameStarted;
    }

    private void Update()
    {
        if(GameManager.CurrentGameState == GameState.GameRunning && Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }
    void GameStarted()
    {
        moveToStartText.SetActive(false);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    #region HealhBar
    private void ChangePlayerHealthBar(int health, int maxHealth)
    {
        healthBarSlider.value = health / (float)maxHealth;
        //healthBarSlider.value = .5f;
    }

    private void ChangeBossHealthBar(int health, int maxHealth)
    {
        bossBarSlider.value = health / (float)maxHealth;
    }

    private void EnableBossBar()
    {
        bossBar.SetActive(true);
    }
    #endregion


    #region Pause
    private void Pause()
    {
        Time.timeScale = 0f;
        GameManager.CurrentGameState = GameState.GamePaused;

        pausePanel.SetActive(true);
    }

    public void UnPause()
    {
        Time.timeScale = 1f;

        GameManager.CurrentGameState = GameState.GameRunning;
        pausePanel.SetActive(false);
    }

    #endregion

    private void OnDestroy()
    {
        GameManager.OnGameStart -= GameStarted;
        Player.OnDamageTaken -= ChangePlayerHealthBar;
        Beholder.OnDamageTaken -= ChangeBossHealthBar;
        Beholder.OnBeholderSpawn -= EnableBossBar;
    }
}
