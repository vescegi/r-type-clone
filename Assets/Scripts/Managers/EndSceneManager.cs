using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndSceneManager : MonoBehaviour
{
    [SerializeField] GameObject winObject;
    [SerializeField] GameObject loseObject;

    [SerializeField] string mainMenuSceneName;
    void Start()
    {
        if (GameManager.CurrentGameState == GameState.GameWon)
        {
            winObject.SetActive(true);
        }
        else
        {
            loseObject.SetActive(true);
        }

    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene(mainMenuSceneName);
    }
}
