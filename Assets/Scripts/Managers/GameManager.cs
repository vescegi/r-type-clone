using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    GameStopped,
    GameRunning,
    GamePaused,
    GameWon,
    GameLost
}

public class GameManager : MonoBehaviour
{
    public float VerticalBounds = 4.5f;
    public float HorizontalBounds = 4.5f;

    public float levelSpeed;

    [SerializeField] float levelSize = 100;
    [SerializeField] string gameEndSceneName;
    [SerializeField] GameObject beholderPrefab;
    public float LevelSize { get => levelSize; private set => levelSize = value; }

    private static GameManager instance;
    public static GameManager Instance
    {
        get => instance;
        set => instance = value;
    }
    public Player Player { get; set; } 

    public InfiniteScroll InfiniteScroll { get; set; }

    public float CurrentLevelDistance { get; private set; }

    public static GameState CurrentGameState;

    public bool GameStopped { get => CurrentGameState == GameState.GameStopped || CurrentGameState == GameState.GamePaused; }

    public static event Action OnGameStart;

    private bool beholderSpawned;

    private void Awake()
    {
        Instance = this;
        CurrentLevelDistance = 0;
        CurrentGameState = GameState.GameStopped;
        beholderSpawned = false;
    }

    private void Update()
    {
        if (GameStopped) return;

        CurrentLevelDistance += Time.deltaTime * levelSpeed;

        if (CurrentLevelDistance >= levelSize && !beholderSpawned)
        {
            Instantiate(beholderPrefab, InfiniteScroll.RightBackground);
            beholderSpawned = true;
        }
    }

    public void GameOver(bool gameWon)
    {
        CurrentGameState = gameWon ?  GameState.GameWon : GameState.GameLost;

        SceneManager.LoadScene(gameEndSceneName);
    }

    public void StartGame()
    {
        CurrentGameState = GameState.GameRunning;

        OnGameStart?.Invoke();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position, new Vector3(HorizontalBounds * 2, VerticalBounds * 2));
    }
#endif
}
