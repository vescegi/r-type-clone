using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] Transform enemyFolder;
    [SerializeField] GameObject baseEnemyPrefab;
    
    [SerializeField] EnemyStats[] stats;
    [SerializeField] float minTimeSpawn;
    [SerializeField] float maxTimeSpawn;


    private static EnemySpawner instance;
    public static EnemySpawner Instance { get => instance; private set => instance = value; }

    private ObjectPool<Enemy> enemyPool;

    private float nexTimeSpawn;
    private float time;

    private GameManager gameManager;

    private void Awake()
    {
        instance = this;
        enemyPool = new ObjectPool<Enemy>(OnCreateEnemy, OnGetEnemy, OnReleaseEnemy);
    }

    private void Start()
    {
        gameManager = GameManager.Instance;
        ResetCooldown();
    }

    private void Update()
    {
        if (gameManager.GameStopped) return;

        EnemyCoolDown();
    }

    #region Action for pooling
    public void ReleaseEnemy(Enemy enemy)
    {
        enemyPool.Release(enemy);
    }
    private Enemy OnCreateEnemy()
    {
        GameObject enemy = Instantiate(baseEnemyPrefab, enemyFolder);
        enemy.SetActive(false);

        return enemy.GetComponent<Enemy>();
    }

    private void OnGetEnemy(Enemy enemy)
    {
        enemy.transform.position = transform.position;
        //enemy.InitializeStats(stats[Random.Range(0, stats.Length)]);
        int chosenEnemy = (int)(gameManager.CurrentLevelDistance * stats.Length / gameManager.LevelSize);

        //Debug.Log(chosenEnemy);

        enemy.InitializeStats(stats[chosenEnemy]);

        enemy.gameObject.SetActive(true);
    }

    private void OnReleaseEnemy(Enemy enemy)
    {
        enemy.gameObject.SetActive(false); 
    }
    #endregion

    #region Spawning
    private void SpawnEnemy()
    {
        ResetCooldown();

        if(gameManager.CurrentLevelDistance < gameManager.LevelSize)
            enemyPool.Get();
    }

    private void ResetCooldown()
    {
        time = 0;
        nexTimeSpawn = Random.Range(minTimeSpawn, maxTimeSpawn);
    }

    private void EnemyCoolDown()
    {
        time += Time.deltaTime;

        if (time >= nexTimeSpawn)
            SpawnEnemy();
    }
    #endregion
}
