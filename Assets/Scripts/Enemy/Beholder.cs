using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
public class Beholder : MonoBehaviour
{
    [SerializeField] private int maxHealth;
    [SerializeField] private int damage;
    [SerializeField] private float shootCoolDownSeconds;

    private float beholderPosition = 0.1f;

    private ShootProjectile shootProjectile;
    public int Health { get; private set; }
    private float timeSinceLastShoot;

    private float height;

    public static event UIManager.OnDamageTakenDelegate OnDamageTaken;
    public static event Action OnBeholderSpawn;

    private void Update()
    {
        Shoot();

        float parentX = transform.parent.position.x;
        if (parentX <= beholderPosition)
        {
            GameManager.Instance.InfiniteScroll.StopScroll = true;
            OnBeholderSpawn?.Invoke();
        }
    }

    private void Awake()
    {
        Health = maxHealth;
        timeSinceLastShoot = 0;

        shootProjectile = GetComponent<ShootProjectile>();

        height = GetComponent<BoxCollider2D>().size.y;
        // Debug.Log(height);
    }

    public void InitializeStats(EnemyStats stats)
    {
        this.maxHealth = stats.maxHealth;
        this.Health = stats.maxHealth;
        this.damage = stats.damage;
        this.shootCoolDownSeconds = stats.shootCoolDownSeconds;
    }

    #region Shooting
    private void Shoot()
    {
        timeSinceLastShoot += Time.deltaTime;

        if (timeSinceLastShoot >= shootCoolDownSeconds)
        {
            timeSinceLastShoot = 0;

            Vector2 position = transform.position;

            Vector2 projectilePosition = new Vector2(shootProjectile.Muzzle.position.x, Random.Range(position.y - height/2, position.y + height/2));
            shootProjectile.Shoot(damage, tag, projectilePosition);
        }
    }

    public void TakeDamage(int damage)
    {
        Health -= damage;

        if (Health <= 0)
        {
            Health = 0;
            Die();
        }

        OnDamageTaken?.Invoke(Health, maxHealth);
        // Debug.Log(Health);
    }

    private void Die()
    {
        GameManager.Instance.GameOver(true);
    }
    #endregion  

}
