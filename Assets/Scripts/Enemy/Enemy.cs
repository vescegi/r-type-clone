using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using Random = UnityEngine.Random;

[Serializable]
public struct EnemyStats
{
    public int maxHealth;
    public int damage;
    public float horizontalSpeed;
    public float verticalSpeed;
    public float shootCoolDownSeconds;
    public Sprite sprite;
}

public class Enemy : MonoBehaviour
{

    //[SerializeField] private EnemyStats stats;
    [SerializeField] private SpriteRenderer spriteRenderer;
    //[SerializeField, Tooltip("Distance from player to despawn")] private float distanceToDespawn;

    private int maxHealth;
    private int damage;
    private float horizontalSpeed;
    private float verticalSpeed;
    private float shootCoolDownSeconds;
    private Sprite sprite;

    private ShootProjectile shootProjectile;
    public int Health { get; private set; }
    private float timeSinceLastShoot;

    private float horizontalSeed;
    private float verticalSeed;

    private float height;
    private float halfWidth;
    private float verticalBounds;
    //private float sqrDistanceToDespawn;

    private GameManager gameManager;
    private void Awake()
    {
        Health = maxHealth;
        timeSinceLastShoot = 0;

        shootProjectile = GetComponent<ShootProjectile>();

        BoxCollider2D boxCollider2D = GetComponent<BoxCollider2D>();
        height = boxCollider2D.size.y;
        halfWidth = boxCollider2D.size.x / 2;
        // Debug.Log(height);

        gameManager = GameManager.Instance;

        horizontalSeed = Random.Range(0f, 1000f);
        verticalSeed = Random.Range(0f, 1000f);
    }

    private void Start()
    {
        verticalBounds = GameManager.Instance.VerticalBounds - height / 2;
        //sqrDistanceToDespawn = distanceToDespawn * distanceToDespawn;
    }

    private void Update()
    {
        MovePerlinNoise();
        Shoot();
        CheckBounds();
    }

    public void InitializeStats(EnemyStats stats)
    {
        this.maxHealth = stats.maxHealth;
        this.Health = stats.maxHealth;
        this.damage = stats.damage;
        this.horizontalSpeed = stats.horizontalSpeed;
        this.verticalSpeed = stats.verticalSpeed;
        this.shootCoolDownSeconds = stats.shootCoolDownSeconds;
        this.spriteRenderer.sprite = stats.sprite;

        Vector3 position = transform.position;

        position.y = Random.Range(-verticalBounds, verticalBounds);
        transform.position = position;
        // Debug.Log(sprite);
    }

    #region Shooting
    private void Shoot()
    {
        timeSinceLastShoot += Time.deltaTime;

        if (timeSinceLastShoot >= shootCoolDownSeconds)
        {
            timeSinceLastShoot = 0;
            shootProjectile.Shoot(damage, tag);
        }
    }

    public void TakeDamage(int damage)
    {
        Health -= damage;

        if (Health <= 0)
        {
            Health = 0;
            Die();
        }

        // Debug.Log(Health);
    }

    private void Die()
    {
        gameObject.SetActive(false);
    }
    #endregion  

    #region Movement
    private void MoveToPlayer()
    {
        Transform target = GameManager.Instance.Player.transform;
        Vector3 direction = (target.position - transform.position).normalized;

        transform.position += direction * (horizontalSpeed * Time.deltaTime);

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle + 180, Vector3.forward);
    }

    private float t = 0;
    private void MovePerlinNoise()
    {
        
        float newY = (Mathf.PerlinNoise(horizontalSeed, verticalSeed + t * verticalSpeed) * 2 - 1) * verticalBounds;
        transform.position = new Vector3(transform.position.x - horizontalSpeed * Time.deltaTime, newY);

        //if((player.transform.position - transform.position).sqrMagnitude > sqrDistanceToDespawn)
        //{
        //    // Debug.Log((transform.position - player.transform.position));
        //    enemySpawner.ReleaseEnemy(this);
        //}

        t += Time.deltaTime;
    }

    private void CheckBounds()
    {
        if (transform.position.x + halfWidth <= -gameManager.HorizontalBounds)
            gameManager.GameOver(false);
    }
    #endregion
}
