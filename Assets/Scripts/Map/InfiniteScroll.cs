using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteScroll : MonoBehaviour
{
    private float scrollSpeed = 1;

    [SerializeField] Transform background1;
    [SerializeField] Transform background2;

    Transform leftBackground;
    public Transform RightBackground { get; private set; }

    private float distance;

    public bool StopScroll;

    GameManager gameManager;

    private void Start()
    {
        StopScroll = false;
        gameManager = GameManager.Instance;
        gameManager.InfiniteScroll = this;
        
        distance = background2.position.x - background1.position.x;

        //Debug.Log(distance);

        leftBackground = background1;
        RightBackground = background2;
    }

    void Update()
    {
        if (gameManager.GameStopped || StopScroll) return;

        scrollSpeed = gameManager.levelSpeed;

        MoveBackgrounds();
        CheckBackgrounds();
    }

    private void MoveBackgrounds()
    {
        background1.Translate(Vector2.left * scrollSpeed * Time.deltaTime);
        background2.Translate(Vector2.left * scrollSpeed * Time.deltaTime);
    }

    void CheckBackgrounds()
    {
        if (leftBackground.position.x < (-GameManager.Instance.HorizontalBounds - distance / 2))
        {
            leftBackground.position = new Vector2(RightBackground.position.x + distance, leftBackground.position.y);

            (leftBackground, RightBackground) = (RightBackground, leftBackground);
        }
    }
}
